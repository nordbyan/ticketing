import { ExpirationCompleteEvent, Publisher, Subjects } from "@nordbyandreas_tickets/common";


export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent>{
    subject: Subjects.ExpirationComplete = Subjects.ExpirationComplete;
}