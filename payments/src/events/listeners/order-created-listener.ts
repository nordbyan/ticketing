import { Listener, OrderCreatedEvent, OrderStatus, Subjects } from "@nordbyandreas_tickets/common";
import { Message } from "node-nats-streaming";
import { Order } from "../../models/order";
import { QUEUE_GROUP_NAME } from "./queue-group-name";


export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
    subject: Subjects.OrderCreated = Subjects.OrderCreated;
    queueGroupName: string = QUEUE_GROUP_NAME;

    async onMessage(data: OrderCreatedEvent['data'], msg: Message) {
        const order = Order.build({
            id: data.id,
            version: data.version,
            userId: data.userId,
            price: data.ticket.price,
            status: data.status
        });

        await order.save();
        
        msg.ack();
    }
}