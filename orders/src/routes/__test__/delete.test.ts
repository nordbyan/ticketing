import { OrderStatus } from '@nordbyandreas_tickets/common';
import request from 'supertest';
import {app} from '../../app';
import { Order } from '../../models/orders';
import { Ticket } from '../../models/ticket';
import {natsWrapper} from '../../nats-wrapper';
import mongoose from 'mongoose';

it('marks an order as cancelled', async () => {

    //create a ticket with ticket model
    const ticket = Ticket.build({
        id: new mongoose.Types.ObjectId().toHexString(),
        title: 'concert', 
        price: 20
    });
    await ticket.save();
    const user = global.signin();

    // make req to create order
    const {body: order} = await request(app)
    .post('/api/orders')
    .set('Cookie', user)
    .send({ticketId: ticket.id})
    .expect(201);

    // make request to cancel order
    await request(app)
    .delete(`/api/orders/${order.id}`)
    .set('Cookie', user)
    .send()
    .expect(204);

    // make sure that ticket is cancelled
    const updatedOrder = await Order.findById(order.id);
    expect(updatedOrder!.status).toEqual(OrderStatus.Cancelled);
});

it('emits an order cancelled event', async () => {

    //create a ticket with ticket model
    const ticket = Ticket.build({
        id: new mongoose.Types.ObjectId().toHexString(),
        title: 'concert', 
        price: 20
    });
    await ticket.save();
    const user = global.signin();

    // make req to create order
    const {body: order} = await request(app)
    .post('/api/orders')
    .set('Cookie', user)
    .send({ticketId: ticket.id})
    .expect(201);

    // make request to cancel order
    await request(app)
    .delete(`/api/orders/${order.id}`)
    .set('Cookie', user)
    .send()
    .expect(204);

    // make sure that event is emitted
    expect(natsWrapper.client.publish).toHaveBeenCalled();
});