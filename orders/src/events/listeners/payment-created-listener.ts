import { Listener, OrderStatus, PaymentCreatedEvent, Subjects } from "@nordbyandreas_tickets/common";
import { QUEUE_GROUP_NAME } from "./queue-group-name";
import {Message} from 'node-nats-streaming';
import { Order } from "../../models/orders";

export class PaymentCreatedListener extends Listener<PaymentCreatedEvent> {
    subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
    queueGroupName = QUEUE_GROUP_NAME;

    async onMessage(data: PaymentCreatedEvent['data'], msg: Message) {
        const {id, orderId, stripeId} = data;
       
        const order = await Order.findById(orderId);
        if(!order) {
            throw new Error('Order not found');
        }
        order.set({status: OrderStatus.Complete})
        await order.save();

        msg.ack();
    }
}