import { Publisher, OrderCancelledEvent, Subjects } from "@nordbyandreas_tickets/common";

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
    subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
}