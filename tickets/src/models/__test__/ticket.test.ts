import { Ticket } from "../ticket";

it('implements optimistic concurrency control', async () => {
    // create an instance of a ticket
    const ticket = Ticket.build({
        title: 'concert',
        price: 5,
        userId: '123'
    });

    // save ticket to database
    await ticket.save();

    // fetch ticket twice
    const first = await Ticket.findById(ticket.id);
    const second = await Ticket.findById(ticket.id);

    // make two separate changes to the ticket we fetched
    first!.set({price: 10});
    second!.set({price: 15});

    // save the first fetched ticket
    await first!.save();

    // save the second fetches ticket and expect an error due to outdated version
    await expect(second!.save()).rejects.toThrow();
 
});


it('increments version number', async () => {
    const ticket = Ticket.build({
        title: 'concert',
        price: 5,
        userId: '123'
    });

    await ticket.save();
    expect(ticket.version).toEqual(0);

    await ticket.save();
    expect(ticket.version).toEqual(1);

    await ticket.save();
    expect(ticket.version).toEqual(2);
 
});