import request from 'supertest';
import {app} from '../../app';
import mongoose from 'mongoose';
import {natsWrapper} from '../../nats-wrapper';
import { Ticket } from '../../models/ticket';


it('returns 404 if provided id does not exist', async () => {
    const id = new mongoose.Types.ObjectId().toHexString();
    await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', global.signin())
    .send({
        title: 'akijfdei',
        price: 20
    })
    .expect(404);
});

it('returns 401 if user not authenticated', async () => {
    const id = new mongoose.Types.ObjectId().toHexString();
    await request(app)
    .put(`/api/tickets/${id}`)
    .send({
        title: 'akijfdei',
        price: 20
    })
    .expect(401);
});

it('returns 401 if user does not own ticket', async () => {

    const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', global.signin())
    .send({title: 'title', price: 100})
    .expect(201);

    const id = new mongoose.Types.ObjectId().toHexString();
    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', global.signin())
    .send({
        title: 'akijfdei',
        price: 20
    })
    .expect(401);
});

it('returns 400 if provided invalid title or price', async () => {
    const cookie = global.signin();
    const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({title: 'title', price: 100})
    .expect(201);

    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({
        title: '',
        price: 20
    })
    .expect(400);

    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({
        title: 'rgrgrggr',
        price: -29
    })
    .expect(400);
});


it('updates the ticket if provided valid inputs', async () => {
    const cookie = global.signin();

    const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({title: 'title', price: 100})
    .expect(201);

    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({
        title: 'title valid two',
        price: 30
    })
    .expect(200);

    const ticketResponse = await request(app)
    .get(`/api/tickets/${response.body.id}`)
    .send();

    expect(ticketResponse.body.title).toEqual('title valid two');
    expect(ticketResponse.body.price).toEqual(30);
});

it('publishes an event', async () => {
    const cookie = global.signin();

    const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({title: 'title', price: 100})
    .expect(201);

    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({
        title: 'title valid two',
        price: 30
    })
    .expect(200);

    expect(natsWrapper.client.publish).toHaveBeenCalled();
});


it('rejects editing reserved tickets', async () => {
    const cookie = global.signin();

    const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({title: 'title', price: 100})
    .expect(201);

    const ticket = await Ticket.findById(response.body.id);
    ticket!.set({orderId: new mongoose.Types.ObjectId().toHexString()});
    await ticket!.save();

    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({
        title: 'title valid two',
        price: 30
    })
    .expect(400);
});